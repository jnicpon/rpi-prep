#!/usr/bin/fish
# This script prepares a RPi for k3s installation.

echo -e \r
echo "Installing Required Packages..."
echo -e \r
sudo apt-get update ; sudo apt-get install iptables ebtables arptables -y
echo -e \r
echo "Configure Legeacy Firewall Dependencies"
echo -e \r
sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
sudo update-alternatives --set arptables /usr/sbin/arptables-legacy
sudo update-alternatives --set ebtables /usr/sbin/ebtables-legacy
echo -e \r
echo "Configure Kernel Cgroups..."
echo -e \r
sudo sed -i '1!b;s/$/\ cgroup_memory=1 cgroup_enable=memory/g' /boot/firmware/cmdline.txt
echo -e \r
echo "Disable Swap..."
echo -e \r
sudo dphys-swapfile swapoff
sudo dphys-swapfile uninstall
sudo systemctl stop dphys-swapfile.service
sudo systemctl disable dphys-swapfile.service
sudo systemctl mask dphys-swapfile.service
echo -e \r
echo "Reboot in 5 mins to complete k3s preparation activities..."
echo -e \r
sudo shutdown -r 5