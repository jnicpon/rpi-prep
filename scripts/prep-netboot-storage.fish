#!/usr/bin/fish
# This script prepares Raspberry Pi storage for network boot use. 
# This script is interactive.

#Set Required Variables / File Paths
set scriptversion 1.1.0
set parentdirectory (pwd)
set initiatorFile /etc/iscsi/initiatorname.iscsi
set piSerial (grep Serial /proc/cpuinfo | tail -c 9)
set kernelversion (uname -r)
set distroinfo (lsb_release -d 2>/dev/null)

# Function to show and set iSCSI Iniatator 
function ipaddr
    if test -e /sys/class/net/eth0/operstate
        ip a s eth0 | egrep -o 'inet [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | cut -d' ' -f2
    else
        echo -e "\033[1mDevice eth0 does not exist.\033[0m"
    end
end

function initiator
    if test -e $initiatorFile
        set -g iscsiinitiator (sudo cat $initiatorFile | grep InitiatorName= | awk '{print substr($1,15)}')
        echo $iscsiinitiator
    else
        echo -e "\033[1mNot Detected!\033[0m"
    end
end

# Function to show RPi Serial Number
function piSerialCheck
    if test -z $piSerial
        echo -e "\033[1mNot Detected!\033[0m"
    else
        echo $piSerial
    end
end

# Initial Greeting
echo -e "Script Version: $scriptversion"
echo -e \r
echo "***************************************************************************"
echo -e "\033[1mWARNING:\033[0m Use this script at your own risk, as the author may not be held"
echo -e "responsible for any unintended consequences and/or data loss!\n"
echo -e "This script is intended to help users quickly prepare Raspberry PIs to"
echo -e "boot via NFS/iSCSI.\n"
echo -e "This script will write a new file system to /dev/sda. Do not continue"
echo -e "unless you are running this script from an Raspberry Pi's SDCARD and have"
echo -e "prepared your iSCSI storage target in advance. Please refer to related blog"
echo -e "articles at \033[1mhttps://warmestrobot.com\033[0m for prerequisite setup instructions."
echo -e "If prompts are not populating as you run this script, abort it vs. hoping it"
echo -e "will work."
echo "***************************************************************************"
read -n1 -p 'echo -e "\033[1mPress any key to continue...\033[0m"'
echo -e \r
echo -e \r
echo -e "\033[1mThis script has only been tested and verified on"
echo -e " Raspberry Pi OS Debian 12 (bookworm).\033[0m"
echo -e \r
echo -e "The following distribution was detected:"
echo -e "\033[1m$distroinfo\033[0m"
echo -e \r
echo -e "You are currently executing this script on \033[1m$hostname\033[0m"
echo -e "This script is running on Fish Shell \033[1mv$version\033[0m"
echo -e \r
echo -e "\033[1mRequired:\033[0m This script must be executed from a user account with"
echo -e "root sudo privileges."
echo -e \r
echo "Raspberry Pi Serial Number:" (piSerialCheck)
echo "Current IPv4 Address:" (ipaddr)
echo

# We need to get some input from the user about the required action to take, along with the logic to execute the approariate required actions.
read -l -p 'echo "Do you wish to setup a new Raspberry Pi to boot from iSCSI? [y/N] "' setupboot
if test $setupboot = 'Y' -o $setupboot = 'y'
    echo "Refreshing apt cache and ensuring dependencies are installed"
    sudo apt-get update ; sudo apt-get -y install nfs-common open-iscsi rsync
    echo -e \r
    read -l -p 'echo -e "What is your full tftpboot NFS mount path?\n(ex: 192.168.1.100:/volume1/tftpboot): "' nfspath
    echo -e \r
    read -l -p 'echo -e "What is your iSCSI target IP?\n(ex: 192.168.1.100): "' iscsitargetip
    echo -e \r
    read -l -p 'echo -e "What is your iSCSI target name?\n(ex: iqn.2000-01.com.synology:NAS.raspberry-01.2fdf1b5d6f): "' iscsitargetname
    echo -e \r
    echo "Current iSCSI Initiator Name:" (initiator)
    echo "Full tftpboot NFS Export Path:" $nfspath
    echo "iSCSI Target IP:" $iscsitargetip
    echo "iSCSI Target Name:" $iscsitargetname
    echo -e \r
    read -l -p 'echo "Are the settings above correct? [y/N] "' setupconfirm
    if test $setupconfirm = 'Y' -o $setupconfirm = 'y'
        echo "Discovering iSCSI Targets..."
        sudo iscsiadm -m discovery -t sendtargets -p $iscsitargetip
        echo "Logging into Target..."
        sudo iscsiadm -m node -l -T $iscsitargetname -p $iscsitargetip
        echo "Enable initramfs hooks..."
        sudo touch /etc/iscsi/iscsi.initramfs
        echo "Generating initramfs..."
        sudo update-initramfs -v -k $kernelversion -c
        echo "Mounting tftpboot NFS path..."
        sudo mount -t nfs -o defaults $nfspath /mnt
        echo "Creating directory for RPI on tftpboot NFS mount..."
        sudo mkdir /mnt/$piSerial
        echo "Copying bootcode to root of tftpboot NFS mount..."
        sudo cp /boot/firmware/bootcode.bin /mnt
        echo "rsync /boot to RPI NFS directory..."
        sudo rsync -avhP /boot/ /mnt/$piSerial
        echo "Writing ext4 file system to /dev/sda..."
        sudo mkfs.ext4 /dev/sda
        echo "Recording UUID of /dev/sda..."
        set iscsiuuid (sudo blkid -o value /dev/sda | awk 'NR==1{print $1}')
        echo "Creating cmdline.txt with required variables..."
	    sudo fish -c "echo 'dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 ip=::::$hostname:eth0:dhcp root=UUID=$iscsiuuid rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait ISCSI_INITIATOR=$iscsiinitiator ISCSI_TARGET_NAME=$iscsitargetname ISCSI_TARGET_IP=$iscsitargetip ISCSI_TARGET_PORT=3260 rw' > /mnt/$piSerial/firmware/cmdline.txt"
        echo "Enabling Activity LED Heartbeat Mode..."
        sudo fish -c "echo "dtparam=act_led_trigger=heartbeat" >> /mnt/$piSerial/firmware/config.txt"
        echo "Enabling iSCSI-enabled initramfs image..."
        sudo fish -c "echo 'initramfs initrd.img-$kernelversion followkernel' >> /mnt/$piSerial/firmware/config.txt"
        sudo rm /mnt/$piSerial/issue.txt
        sudo rm /mnt/$piSerial/config.txt
        sudo rm /mnt/$piSerial/cmdline.txt
        echo "Creating Required Symbolic Links for Network Boot"
        cd /mnt/$piSerial
        sudo cp -s firmware/* .
        cd $parentdirectory
	    echo "Unmounting NFS..."
        sudo umount /mnt
        echo "Mounting iSCSI Root Volume..."
        sudo mount -t ext4 -o defaults /dev/sda /mnt
        echo "rsync SDCARD root volume to iSCSI Root Volume..."
        sudo rsync -avhP --exclude /boot --exclude /proc --exclude /sys --exclude /dev --exclude /mnt / /mnt/
        sudo mkdir /mnt/{dev,proc,sys,boot,mnt}
        echo "Modify fstab on iSCSI Root Volume..."
        sudo sed -i 's/PARTUUID/#PARTUUID/g' /mnt/etc/fstab
        sudo sed -i "2i $nfspath/$piSerial /boot nfs _netdev 0 0" /mnt/etc/fstab
        sudo sed -i "3i UUID=$iscsiuuid / ext4 defaults,_netdev 1 1" /mnt/etc/fstab
        echo -e \r
        echo -e \r
        echo "****************"
        echo "Script Complete!"
        echo "****************"
        echo -e \r
        echo -e \r
        echo "Please shutdown and attempt to netboot without your SDCARD!"
    else
        echo -e \r
        echo -e \r
        echo "****************"
        echo "Aborting Script!"
        echo "****************"
        echo -e \r
        echo -e \r
    end
else
    echo -e \r
    echo -e \r
    echo "****************"
    echo "Aborting Script!"
    echo "****************"
    echo -e \r
    echo -e \r
end