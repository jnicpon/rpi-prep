# rpi-prep

This project contains shell scripts written for Raspberry Pi OS in fish shell that automate a variety of complicated administrative tasks.

## Version

Current Version: **1.1.1**

## Changes

- 1.1.1 - Maintenance release to correct documentation typos and formatting issues.
- 1.1.0 - Revised scripts to support Raspberry Pi OS Bookworm, as well as removed deprecated scripts that are no longer relevant.
- 1.0.0 - Initial release for pre-Bookworm Raspberry Pi OS.

## Description

Inside the scripts folder, this project contains the following Fish Shell scripts:

1. **prep-k3s-install.fish** - Non-interactive script that modifies RPis to meet the current prerequisites in order to support K3s.
2. **prep-netboot-storage.fish** - Interactive script to bootstrap an RPi SDCARD to Netboot via NFS/iSCSI.

## Installation

The only current prerequisite to *run* these scripts is **Fish Shell v3.0+**; however, some scripts may automatically install additional dependencies.

While I recommend using Fish Shell over bash, zsh, and other shells as a default, I understand that this is a very personal preference for most folks, and some folks dislike modifying defaults (ex: bash). All of that said, you should only have to clone the repo or manually download the individual scripts that you require and then make them executable with `chmod +x`. Afterwards, simply execute the script in your favorite shell as long as Fish shell is installed locally. I chose to write these scripts in Fish Shell in order to help them be a little more approachable to less technical folks and users new to Linux and scripting in general.

## Usage

These scripts (v1.1.x) require adding Fish Shell to Raspberry Pi OS; however, they were designed for, and tested exclusively on the lite version of Raspberry Pi OS 64-bit (Debian 12 "Bookworm") which requires RPi 3 or higher. While the 64-bit OS version also supports the newer RPI Zero 2 W, that device lacks a physical ethernet adapter to make booting off of the network from NFS/iSCSI feasible. In addition, if you are not using Debian 12, please use the initial release of these scripts to support legacy versions of Raspberry Pi OS.

To add Fish Shell:

```text
sudo apt-get update
sudo apt-get install fish
```

To execute a fish shell script within another shell:

```text
sudo fish </path/to/script.fish>
```

## Support

The scripts are provided to the community to make it easier for folks to setup Raspberry Pi OS for network boot.

To request support from the author, please file an issue on this GitLab project if you find bugs, issues, or have additional feedback.

## Roadmap

Updates will be published to support specific versions of the scripts in relation to Raspberry Pi OS changes.

## Contributing

If you are interested in contributing scripts or enhancements, please feel free to fork the project and submit merge requests.

## Authors and acknowledgment

This project and these scripts are written and maintained by John Nicpon.

This update corresponds to a recent blog post on [Warmest Robot + Warmest Human](https://warmestrobot.com).

The following blog articles inspired many of these underlying scripts:

- [Raspberry Pi Documentation - Raspberry Pi Hardware](https://www.raspberrypi.org/documentation/hardware/raspberrypi/bootmodes/net_tutorial.md)
- [Net boot (PXE + iSCSI) with a RaspberryPi 3 - darknao's stuff](https://stuff.drkn.ninja/post/2016/11/08/Net-boot-(PXE-iSCSI)-with-a-RaspberryPi-3)

## License

### MIT License

Copyright 2024 John Nicpon

Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
